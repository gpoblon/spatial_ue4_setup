CURRENT BUILD: UE4.25.3 / SpatialOsGDK 0.11 (TODO: update the following for every new version)

See official improbable documentation for detailed setups:
https://documentation.improbable.io/gdk-for-unreal/docs/

## Setup SpatialOS UE build:

The following steps require to have the following setup:
- an Improbable account: https://improbable.io/
- a GitHub account with SSH setup
- be part of Epic Games GitHub organization

### STEP 1: DEPENDENCIES
Note: all these executables have been put directly into the `installers` folder.
Note: There is no need to install UnrealEngine from the official Epic Games Launcher

I recommend you to have a `GameDev` folder as close as your root disk as possible ; that includes a `projects` folder and a `spatialOsUE` (your forked engine installation) folder.
Name it whatever you want but remember to have the fork install somewhere you have access to so you can modify it.
Every other software can be installed like usual in your `Program Files`

- Install Git: https://github.com/git-for-windows/git/releases/download/v2.29.2.windows.3/Git-2.29.2.3-64-bit.exe (and optionnaly a git client)
	- Optionally if you are planning on using git to sync your projects, I recommend installing Git LFS
- Install DirectX runtime: https://www.microsoft.com/en-us/download/confirmation.aspx?id=8109
- Install the cross compiling toolchain (4.25 is compatible with v16): http://cdn.unrealengine.com/CrossToolchain_Linux/v16_clang-9.0.1-centos7.exe
- Install SpatialOs: https://console.improbable.io/installer/download/stable/latest/win
- Install Visual Studio 2019: https://visualstudio.microsoft.com/fr/thank-you-downloading-visual-studio/?sku=Community&rel=16
	You will need to install the following module, which takes quite some space:
	- Universal Windows Platform development
	- .NET desktop development
		- Manually add .NET Framework 4.6.2 development tools from the entire list
	- Desktop development with C++
- Clone the SpatialOs UE4 fork: `git clone https://github.com/improbableio/UnrealEngine.git spatial_unreal_engine` from the installed command `git bash` or from the installed client
	From with, do the following:
	- Run `Setup.bat`
	- Run `GenerateProjectFiles.bat`
	- Run `InstallGDK.bat`
	- Open `UE4.sln` which will start Visual Studio > Right-click on the solution > Generate (/ Build) (can take several hours)
	
### STEP 2: SET UP A PROJECT
- Open the UnrealEngine forked Editor: `UnrealEngine/Engine/Binaries/Win64/UE4Editor.exe` (tip: make a shortcut of it)
- Create a new project from the SpatialOsGDK template with the following settings: `C++` / `No Starter Content` and put it in your list of Unreal Projects
- This will open up Visual Studio: right-click on the solution and generate it (takes like 10 minutes)
- Start the `Local Windows Debugger` (F5)

You're all setup! Now to start your game, first click on generate a schema and start the local deployment.

### Go further by setting your Cloud Deployment:

To start a cloud deployment, get the name of the single SpatialOs instance that has been granted to you for free (see https://console.improbable.io/, the name looks like this: `beta_randomword_anotherword_randomnumber`)
Then go back to your UEEditor and set the 'Cloud Deployment Configuration' like this:
	- Project Name is the SpatialOs instance name your just got
	- give an assembly (program) name and deployment name of your choice. Make it logical and unique, like: `asm_project_dev_internal_playtest_00`
	- Start the deployment. Note that the assembly will take time to build (up to 2 hours)
	- From the console you can share your game program by a link. Testers will have to install the SpatialOsUE4Fork (?)

#### Helper scripts from SpatialOS
| Helper script  | Description |
| --- | --- |
| `LaunchClient.bat` | Launches a client. Requires a SpatialOS deployment and a managed or manually launched server|
| `LaunchServer.bat` | Launches an Unreal dedicated server on ThirdPersonExampleMap. Requires a SpatialOS deployment |
| `LaunchSpatial.bat` | Launches a SpatialOS deployment with default launch config (found in `spatial/default_launch.json`) |
| `ProjectPaths.bat` | Used by the `LaunchClient.bat`, `LaunchServer.bat` and `LaunchSpatial.bat` to specify the project environment when those scripts are run |